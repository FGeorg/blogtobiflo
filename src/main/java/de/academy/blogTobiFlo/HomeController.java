package de.academy.blogTobiFlo;

import de.academy.blogTobiFlo.entry.Entry;
import de.academy.blogTobiFlo.entry.EntryRepository;
import de.academy.blogTobiFlo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

@Controller
public class HomeController {

    private EntryRepository entryRepository;

    @Autowired
    public HomeController(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    @GetMapping("/home")
    public String home(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        List<Entry> entryList = entryRepository.findAllByOrderByPostedAtDesc();
        model.addAttribute("entryList", entryList);

        return "home";

    }

}

