package de.academy.blogTobiFlo.entry;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntryRepository extends CrudRepository<Entry, Long> {

    List<Entry> findAllByOrderByPostedAtDesc();

    Entry findById(long id);


}
