package de.academy.blogTobiFlo.entry;


import de.academy.blogTobiFlo.comment.Comment;
import de.academy.blogTobiFlo.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Entry {

    private String text;
    private String headline;
    private Instant postedAt;

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "entry")
    private List <Comment> comments;

    public Entry(String text, String headline, Instant postedAt) {
        this.text = text;
        this.headline = headline;
        this.postedAt = postedAt;
    }

    public Entry(String text, String headline, Instant postedAt, User user) {
        this.text = text;
        this.headline = headline;
        this.postedAt = postedAt;
        this.user = user;
    }

    public Entry() {
    }

    public String getText() {
        return text;
    }

    public String getHeadline() {
        return headline;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public long getId() {
        return id;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
