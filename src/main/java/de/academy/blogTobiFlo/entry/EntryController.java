package de.academy.blogTobiFlo.entry;

import de.academy.blogTobiFlo.comment.Comment;
import de.academy.blogTobiFlo.comment.CommentDTO;
import de.academy.blogTobiFlo.comment.CommentRepository;
import de.academy.blogTobiFlo.user.User;
import de.academy.blogTobiFlo.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.Instant;


@Controller
public class EntryController {

    private EntryRepository entryRepository;


    @Autowired
    public EntryController(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;

    }


    @GetMapping("/entry")
    public String writeEntry(Model model) {
        model.addAttribute("writeentry", new EntryDTO("",""));
        return "entry";}


    @PostMapping("/entry")
    public String entry(@ModelAttribute ("writeentry") EntryDTO entryDTO, @ModelAttribute ("sessionUser") User sessionUser){
    Entry entry = new Entry(entryDTO.getText(),entryDTO.getHeadline(),Instant.now(), sessionUser);
    entryRepository.save(entry);

    return "redirect:/home";}
}