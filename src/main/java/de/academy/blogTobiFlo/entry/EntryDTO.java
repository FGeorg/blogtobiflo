package de.academy.blogTobiFlo.entry;


public class EntryDTO {

    private String text;
    private String headline;


    public EntryDTO(String text, String headline) {
        this.text = text;
        this.headline = headline;

    }


    public String getText() {
        return text;
    }

    public String getHeadline() {
        return headline;
    }

}
