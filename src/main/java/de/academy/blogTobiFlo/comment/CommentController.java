package de.academy.blogTobiFlo.comment;

import de.academy.blogTobiFlo.entry.Entry;
import de.academy.blogTobiFlo.entry.EntryRepository;
import de.academy.blogTobiFlo.user.User;
import de.academy.blogTobiFlo.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.Instant;

@Controller
public class CommentController {

private EntryRepository entryRepository;
private CommentRepository commentRepository;



@Autowired
public CommentController(EntryRepository entryRepository, CommentRepository commentRepository){
    this.entryRepository = entryRepository;
    this.commentRepository = commentRepository;

    }

    @GetMapping("/comment/{id}")
    public String comment(@PathVariable long id, Model model) {
    Entry entry = entryRepository.findById(id);
    model.addAttribute("entry", entry);
    model.addAttribute("comment", new CommentDTO(""));

    return "comment";
}

    @PostMapping("/comment/{id}")
    public String comment(@ModelAttribute("comment") CommentDTO commentDTO, @PathVariable long id, @ModelAttribute ("sessionUser") User sessionUser) {
    Comment comment = new Comment(commentDTO.getText(), Instant.now(), entryRepository.findById(id), sessionUser);
    commentRepository.save(comment);

    return "redirect:/home";
    }






}
