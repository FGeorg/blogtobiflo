package de.academy.blogTobiFlo.comment;

import de.academy.blogTobiFlo.entry.Entry;
import de.academy.blogTobiFlo.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    private long id;

    private String text;
    private Instant postedAt;


    @ManyToOne
    private Entry entry;

    @ManyToOne
    private User user;

    public Comment() {
    }

    public Comment(String text, Instant postedAt) {
        this.text = text;
        this.postedAt = postedAt;
    }

    public Comment(String text, Instant postedAt, Entry entry, User user) {
        this.text = text;
        this.postedAt = postedAt;
        this.entry = entry;
        this.user = user;
    }


    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }


    public Entry getEntry() {
        return entry;
    }

}
