package de.academy.blogTobiFlo.comment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    Comment findById(long id);

    List<Comment> findAllById(long id);



}
