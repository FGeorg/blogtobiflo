package de.academy.blogTobiFlo.admin;

import de.academy.blogTobiFlo.user.User;
import de.academy.blogTobiFlo.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class AdminController {

private UserRepository userRepository;

@Autowired
public AdminController (UserRepository userRepository){
    this.userRepository = userRepository;
}

@GetMapping("/admin")
    public String admin(Model model){

    List<User> userListRegistered = userRepository.findByAdmin(false);
    List<User> userListAdmin = userRepository.findByAdmin(true);
    model.addAttribute("userListRegistered", userListRegistered);
    model.addAttribute("userListAdmin", userListAdmin);


return "userlist";}

    
@PostMapping("/admin/{username}")
public String admin(@PathVariable String username) {
User currentUser= userRepository.findByUsername(username);
currentUser.setAdmin(true);
userRepository.save(currentUser);

return "admin";}


}