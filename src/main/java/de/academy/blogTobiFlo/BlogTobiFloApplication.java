package de.academy.blogTobiFlo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogTobiFloApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogTobiFloApplication.class, args);
	}

}
