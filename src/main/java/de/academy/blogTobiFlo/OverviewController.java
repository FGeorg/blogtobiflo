package de.academy.blogTobiFlo;

import de.academy.blogTobiFlo.entry.Entry;
import de.academy.blogTobiFlo.entry.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class OverviewController {

    private EntryRepository entryRepository;

    @Autowired
    public OverviewController(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    @GetMapping("/overview")
    public String showentries(Model model) {

        List<Entry> entryList = entryRepository.findAllByOrderByPostedAtDesc();

        model.addAttribute("entryList", entryList);

        return "overview";
    }


}
