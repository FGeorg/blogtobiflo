package de.academy.blogTobiFlo.user;

import de.academy.blogTobiFlo.comment.Comment;
import de.academy.blogTobiFlo.entry.Entry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {

    private String username;
    private String password;
    private boolean admin;

    @Id
    @GeneratedValue
    private long id;

    @OneToMany (mappedBy = "user")
    private List <Entry> entry;

    @OneToMany (mappedBy = "user")
    private List<Comment> ownComments;

    public User(String username, String password, boolean admin) {
        this.username = username;
        this.password = password;
        this.admin = admin;
    }

    public User(String username, boolean admin){
        this.username = username;
        this.admin = admin;
    }

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public String getUsername() {
        return username;
    }

    public boolean isAdmin() {
        return admin;
    }

    public long getId() {
        return id;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}

