package de.academy.blogTobiFlo.user;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsernameAndPassword(String username, String password);

    boolean existsByUsername(String username);

    List<User>findByAdmin(boolean admin);

    User findByUsername(String username);

    User findById(long id);


}
